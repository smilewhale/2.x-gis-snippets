##Layer=vector

from PyQt4.QtGui import *
import qgis

#set layer to be edited
vl = processing.getObject(Layer)
pr = vl.dataProvider()

#****************This is the section used to create a list of fields that you want to save/not purge

#replace the letters with a list of comma seperated fields you want to save
cols_to_save = ['a','b','d']

#***************************

#this is the array that holds all of the column names in the vector
fields = vl.pendingFields()

#set up array to put the deletable fields into
deletecols = []

#iterate through the fields in the vector layer
for i in range(fields.count()):
    field = fields[i]
    #checks to see if the column is not in the list of columns to save
    if not field.name() in cols_to_save:
        print field.name()
        #add the column to the list of columns to delete if it is not in the save list
        deletecols.append(i)
        
#delete the columns that are in the delete list
pr.deleteAttributes(deletecols)
vl.updateFields()
print 'reduction script is finished running, check attribute table'


#Author: Justin, jus@smilewhale.com
#last edit: 11/27/2018