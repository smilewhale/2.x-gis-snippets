##Layer_Select=vector
##Data_Layer=vector

##client_id=number 0
##field_id=number 0




##yld=group

import qgis
from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *

l = processing.getObject(Layer_Select)
lf = l.pendingFields()
p = l.dataProvider()

d = processing.getObject(Data_Layer)
df = d.pendingFields()
dp = d.dataProvider()

data_cols = []

for f in d.pendingFields():
    data_cols.append(f.name())
    print f.name()

l.startEditing()
for dfeature in d.getFeatures(): 
    nfeature = QgsFeature(l.pendingFields())
    nfeature.setAttribute('client_id',client_id)
    nfeature.setAttribute('field_id',field_id)
    nfeature.setGeometry(dfeature.geometry())
    for col in data_cols:
        if col == 'Field':
            nfeature.setAttribute('fieldname',dfeature.attribute(col))
        if col == 'Dataset':
            nfeature.setAttribute('dataset',dfeature.attribute(col))
        if col == 'Product':
            nfeature.setAttribute('product',dfeature.attribute(col))
        if col == 'Elevation_':
            nfeature.setAttribute('elevation',dfeature.attribute(col))
        if col == 'Time':
            nfeature.setAttribute('time',dfeature.attribute(col))
        if col == 'Swth_Wdth_':
            nfeature.setAttribute('swath_ft',dfeature.attribute(col))
        if col == 'Moisture__':
            nfeature.setAttribute('moisture',dfeature.attribute(col))
        if col == 'Yld_Vol_We':
            nfeature.setAttribute('yldwet_bu',dfeature.attribute(col))
        if col == 'Yld_Vol_Dr':
            nfeature.setAttribute('ylddry_bu',dfeature.attribute(col))
        if col == 'Speed_mph_':
            nfeature.setAttribute('speed_mph',dfeature.attribute(col))
        if col == 'Date':
            nfeature.setAttribute('date',dfeature.attribute(col))
    p.addFeatures([nfeature])
l.commitChanges()