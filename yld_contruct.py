
##yld=group
##Output=output vector

import qgis
from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
import os, sys

    
#path and name for new shapefile
Save_As = Output
print "File location of new shapefile" + Save_As


fieldslist = QgsFields()
fieldslist.append(QgsField("fieldname", QVariant.String, "string",30))
fieldslist.append(QgsField("field_id", QVariant.Int, "integer",10))
fieldslist.append(QgsField("dataset", QVariant.String, "string",200))
fieldslist.append(QgsField("product", QVariant.String, "string",30))
fieldslist.append(QgsField("elevation", QVariant.Double, "Real",10,4))
fieldslist.append(QgsField("time", QVariant.Date))
fieldslist.append(QgsField("swath_ft", QVariant.Double, "Real",10,4))
fieldslist.append(QgsField("moisture", QVariant.Double, "Real",10,4))
fieldslist.append(QgsField("yldwet_bu", QVariant.Double, "Real",10,4))
fieldslist.append(QgsField("ylddry_bu", QVariant.Double, "Real",10,4))
fieldslist.append(QgsField("speed_mph", QVariant.Double, "Real",10,4))
fieldslist.append(QgsField("date", QVariant.Date))






        
sl = processing.getObject(Save_As)
sw = QgsVectorFileWriter(Save_As,"utf-8",fieldslist,QGis.WKBPoint,None,"ESRI Shapefile")


    

if sw.hasError() != QgsVectorFileWriter.NoError:
    print "Error when creating shapefile: ",  sw.errorMessage()
del sw