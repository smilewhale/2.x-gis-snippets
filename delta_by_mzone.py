##Layer=vector

from PyQt4.QtCore import *
import qgis
from qgis.core import *
from qgis.utils import *

#set layer to be edited
vl = processing.getObject(Layer)
pr = vl.dataProvider()


#put the names of the cols in the '' areas, keep the non-'' names the same
streak = 'STREAK'
value = 'VALUE'
delta = 'DELTA'


#columns to add array
addcols_array = [
  QgsField(delta,QVariant.Double,"Real",11,2),
]

#actually adding the columns based on the array
pr.addAttributes(addcols_array)
vl.updateFields()


if vl.type() == QgsMapLayer.VectorLayer:
    vl.startEditing()
    standard_h_values = {}
    standard_c_values = {}
    cold_values = {}
    hot_values = {}
    for row in vl.getFeatures():
        if row[streak] == 'C':
            #add cold value for zone to dictionary with mzoneid being the key
            cold_values[row['MZONE_ID']] = row['VALUE']
            #print just to see it
            print str(row['MZONE_ID'])+' COLD value is:'+ str(cold_values[row['MZONE_ID']])
            
        if row[streak] == 'H':
            #add hot value for zone to dictionary with mzoneid being the key
            hot_values[row['MZONE_ID']] = row['VALUE']
            #print just to see it
            print str(row['MZONE_ID'])+' HOT value is:'+ str(hot_values[row['MZONE_ID']])

    print 11
    for row in vl.getFeatures():
        zoneid = row['MZONE_ID']
        if row['STREAK'] == 'HS':
            deltaval = hot_values[row['MZONE_ID']] - row[value]
            vl.changeAttributeValue(row.id(), pr.fieldNameIndex (delta), deltaval)
        if row['STREAK'] == 'CS':
            deltaval = cold_values[row['MZONE_ID']] - row[value]
            vl.changeAttributeValue(row.id(), pr.fieldNameIndex (delta), deltaval)
        if row['STREAK'] == 'H':
            deltaval = 0
            vl.changeAttributeValue(row.id(), pr.fieldNameIndex (delta), deltaval)
        if row['STREAK'] == 'C':
            deltaval = 0
            vl.changeAttributeValue(row.id(), pr.fieldNameIndex (delta), deltaval)
        
        

    vl.commitChanges()
#Author: Justin, jus@smilewhale.com
#last edit: 11/27/2018

