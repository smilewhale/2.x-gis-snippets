##Layer=vector

from PyQt4.QtCore import *
import qgis
from qgis.core import *
from qgis.utils import *

#set layer to be edited
vl = processing.getObject(Layer)
pr = vl.dataProvider()

#columns to add array, remove what isnt needed, add what does, seperate with commas
addcols_array = [
    QgsField("decimalcol",QVariant.Double,"Real",11,7),
    QgsField("stringcol",QVariant.String,"Text",50),
]
#actually adding the columns based on the array
pr.addAttributes(addcols_array)
vl.updateFields()

#Author: Justin, jus@smilewhale.com
#last edit: 11/27/2018